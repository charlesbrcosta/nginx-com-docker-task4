# Aplicação de Balanceamento de carga com Nginx e Docker.

> :hammer: projeto em desenvolvimento :hammer:

![Badge Licença MIT](https://img.shields.io/badge/license-MIT-orange)
![Badge versao Nginx](https://img.shields.io/badge/nginx-v1.18.0-green)
![Badge versao Docker](https://img.shields.io/badge/Docker-20.10.18-blue)

## Tecnologias utilizadas
-   Nginx;
-   Docker / Docker-compose;
-   Certificado OpenSSL auto-assinado;

## Instalação das Tecnologias e Dependências - (Linux)

Baixando atualização e informações dos pacotes do Linux de todas as fontes 
```
sudo apt update
```

[Clique aqui](https://docs.docker.com/engine/install/ubuntu/) para ter acesso a documentação de instalação do docker no Linux Ubuntu


Este comando irá baixar a imagem do nginx no docker mapeando a porta do container 80 para porta do host 9000

```
docker run -d --rm --name=<nome da imagem>-p <definir porta do host>:80 nginx
```

Criando o volume do meu diretorio atual $(pwd)/html para o caminho do container /usr/share/nginx/html

```
docker run -d --name=<nome da imagem> -p <definir porta do host>:80 -v $(pwd)/html:/usr/share/nginx/html nginx
```
Criando volume do diretorio atual $(pwd) para /data. Dentro de /data estão os arquivos de configuração do nginx. O comando sh permitir executar comandos do shell do linux.

```
docker run -it --name=<nome da imagem>-p <definir porta do host>:80 -v $(pwd):/data  nginx sh
```

Copiando o diretório de configuração do Nginx dentro de /etc

```
cd /etc 
```

```
cp -R nginx /data
```
## Definindo permissão 

Após copiar o diretório de configuração do Nginx é preciso definir as permissões.
como trata-se de uma ambiente de homologação, foi liberado as permissões todas de Execução, leitura e escrita.

```
sudo chmod 777 ssl.conf
```

```
sudo chmod 777 nginx/nginx.conf
```

```
sudo chmod 777 conf.d/default.conf
```

## Criação do certificado digital auto-assinado com o OpenSSL.

```
openssl req -newkey rsa:4096 -nodes -sha256 -keyout certificado.key -x509 -days 365 -out certificado.crt
```

## Configurando arquivo ssl.conf

O diretorio nginx/conf.d foi configurado no arquivo ssl.conf, a porta https 443 e adicionado o caminho do certificado em ssl_certificate

server {
    listen 443 ssl;
    server_name localhost;

    ssl_certificate /cert/certificado.crt
    ssl_certificate_key /cert/certificado.key
}

## Redireciando todas configuração ssl 
As configuração de redirecionamento do ssl para porta 443 foi adicionado no arquivo default.conf.

```
return 301 https://$host$request_uri;
```

## Balanceamento de carga
Foi adicionado no arquivo nginx.conf o upstream para o balanceamento de carga entre servidores web. como exemplo foi utilizado copias da pasta html apenas para simular servidores, como se estivessem em um ambiente de produção. 

```
  upstream all {
        server <ip do host>:9001;
        server <ip do host>:9002;
    }
```

## Configuração do proxy reverse 
Para o proxy reverse foi efetuado as configuração no arquivo ssl.conf

```
    location / {
        #root   /usr/share/nginx/html;
        #index  index.html index.htm;
        proxy_pass http://all/;
    }
```

## Funcionlidades docker-compose.yml

Foram criados 3 servidores. Caso o servidor 1 e 2 entre em manutenção, automaticamente acionará o servidor de backup.

* `Funcionalidade 1` : Criação da Network;
* `Funcionalidade 2` : Baixa da imagem Nginx na última versão;
* `Funcionalidade 3` : Mapeamento de portas e volumes;
* `Funcionalidade 4` : Subindo os servidores.
